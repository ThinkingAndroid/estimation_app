'use strict';
exports.main = async (event, context) => {
	const db = uniCloud.database();
	const dbCmd = db.command;
	const collection = await db.collection('testlist').doc(event.id).update({
		times:dbCmd.inc(1)

	});
	return collection
}
