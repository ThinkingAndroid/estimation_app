'use strict';
exports.main = async (event, context) => {
	const db = uniCloud.database();
	const collection = await db.collection('testlist').skip(event.page).orderBy("title", "desc").get();
	return collection
}
