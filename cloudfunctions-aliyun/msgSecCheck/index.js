var appid = 'wx713d73bed192bf36';
var appsecret = '4c47b2d468a14b1496a7afb3fb85a43c';

// 获取 access_token 值
let tokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appid + '&secret=' +
	appsecret
// 文本内容检测接口
let checkUrl = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token='

// 云函数入口函数
exports.main = async (event, context) => {
	let tokenResponse = await uniCloud.httpclient.request(tokenUrl, {
		method: 'POST',
		data: {
			appid: appid,
			appsecret: appsecret
		},
		dataType: 'json'
	});
	let token = tokenResponse.data.access_token;
	let checkResponse = await uniCloud.httpclient.request(checkUrl + token, {
		method: 'POST',
		data: JSON.stringify({
			"content": event.text
		}),
		dataType: 'json'
	}, );
	return checkResponse

}
