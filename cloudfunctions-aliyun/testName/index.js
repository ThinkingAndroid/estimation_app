'use strict';
exports.main = async (event, context) => {
	let myname = event.myname;
	let hername = event.hername;
	let testId = event.testId;
	let answerId = (myname % 12 + hername % 14).toString();
	const db = uniCloud.database();
	const collection = await db.collection('testAnswer').where({
		answerId: answerId,
		testId:testId
	}).get();
	return collection
};
