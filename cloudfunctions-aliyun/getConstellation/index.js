'use strict';
exports.main = async (event, context) => {
	let male = event.male;
	let female = event.female;
	let testId = event.testId;
	const db = uniCloud.database();
	const collection = await db.collection('answerlist').where({
		male: male,
		female:female
	}).get();
	return collection
};
